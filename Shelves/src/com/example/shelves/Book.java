package com.example.shelves;

public class Book {
	private int id;
	private String bookName;
	private String author;
	private String description;
	private String status;
	private int rating;
	private String image;
	
	public Book()
	{
		this.bookName=null;
		this.author=null;
		this.description=null;
		this.status=null;
		this.rating=0;
		this.image=null;
	}
	
	public Book(String bookName, String author, String description, String status, int rating)
	{
		super();
		this.bookName = bookName;
		this.author = author;
		this.description = description;
		this.status = status;
		this.rating = rating;		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}	
}
