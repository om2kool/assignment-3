package com.example.shelves;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class UpdateActivity extends Activity {
	protected SDBHelper db;
	protected ShelvesActivity s;
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.update_shelves);
		
		Button b = (Button)findViewById(R.id.ButtonBack);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(UpdateActivity.this, ShelvesActivity.class);
				startActivity(i);			
			}
		});
	}
	
	public void updateBook(View button) {
		final EditText nameField = (EditText)findViewById(R.id.EditTextName);
		String name = nameField.getText().toString();
		final EditText authorField = (EditText)findViewById(R.id.EditTextAuthor);
		String author = authorField.getText().toString();
		final EditText descField = (EditText)findViewById(R.id.EditTextDescription);
		String description = descField.getText().toString();
		final Spinner statusSpinner = (Spinner)findViewById(R.id.SpinnerStatus);
		String status = statusSpinner.getSelectedItem().toString();
		final Spinner ratingSpinner = (Spinner)findViewById(R.id.SpinnerRating);
		String rating = ratingSpinner.getSelectedItem().toString();
		int ratingint = Integer.parseInt(rating);
		
		if (name.equalsIgnoreCase("")) {
			Toast.makeText(this, "Enter Book Name!!", Toast.LENGTH_LONG).show();			
		} else if (author.equalsIgnoreCase("")) {
			Toast.makeText(this, "Enter Author Name!!", Toast.LENGTH_LONG).show();
		} else if (description.equalsIgnoreCase("")) {
			Toast.makeText(this, "Enter Description!!", Toast.LENGTH_LONG).show();
		} else if(status.equalsIgnoreCase("")) {
			Toast.makeText(this, "Select Status!!", Toast.LENGTH_LONG).show();
		} else if(rating.equalsIgnoreCase("")) {
			Toast.makeText(this, "Select Rating!!", Toast.LENGTH_LONG).show();
		} else {
			Book book = new Book(name, author, description, status, ratingint);
			db.updateBook(book);			
			Log.d("shelf", "data updated");			
			s.adapt.notifyDataSetChanged();
		}
	}
}
