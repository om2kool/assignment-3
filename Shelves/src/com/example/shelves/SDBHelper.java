package com.example.shelves;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SDBHelper extends SQLiteOpenHelper {
	public final static int VERSION = 1;
    public final static String NAME = "shelf.db";
    public final static String TABLE = "LIST";
    public final static String BOOKID = "id";
    public final static String BOOKNAME = "bookName";
    public final static String AUTHOR = "author";
    public final static String DESCRIPTION = "description";
    public final static String STATUS = "status";
    public final static String RATING = "rating";
    public final static String IMAGE = "image";
    
	public SDBHelper(Context context) {
		super(context, NAME, null, VERSION);		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table " +TABLE +" ("+BOOKID+" integer primary key autoincrement, "+BOOKNAME+" text, "+AUTHOR+" text, "+DESCRIPTION+" text, "+STATUS+" text, "+RATING+" integer, "+IMAGE+" text);";	    
        db.execSQL(sql);		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists "+TABLE);        
        onCreate(db);		
	}
	
	public List<Book> getAllBooks() {
		List<Book> bookList = new ArrayList<Book>();
		
		String selectQuery = "select * from " + TABLE;

		SQLiteDatabase db = this.getWritableDatabase();
		
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to the book list
		if (cursor.moveToFirst()) {
			do {
				Book book = new Book();
				book.setId(cursor.getInt(0));
				book.setBookName(cursor.getString(1));				
				book.setAuthor(cursor.getString(2));
				book.setDescription(cursor.getString(3));
				book.setStatus(cursor.getString(4));
				book.setRating(cursor.getInt(5));
				book.setImage(cursor.getString(6));
				
				bookList.add(book);				
			} while (cursor.moveToNext());
		}

		// return book list
		return bookList;
	}
	
	public void addBook(Book book) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BOOKNAME, book.getBookName());
		values.put(AUTHOR, book.getAuthor());
		values.put(DESCRIPTION, book.getDescription());
		values.put(STATUS, book.getStatus());
		values.put(RATING, book.getRating());			

		// Inserting Row
		db.insert(TABLE, null, values);
		db.close(); // Closing database connection
	}
	
	public void updateBook(Book book) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(BOOKNAME, book.getBookName());
		values.put(AUTHOR, book.getAuthor());
		values.put(DESCRIPTION, book.getDescription());
		values.put(STATUS, book.getStatus());
		values.put(RATING, book.getRating());							
		
		// Inserting Row
		db.update(TABLE, values, BOOKID +" = ?", new String[]{String.valueOf(book.getId())});			
		db.close(); // Closing database connection
	}
	
	public void removeBook(Book book) {
		SQLiteDatabase db = this.getWritableDatabase();		
		db.delete(TABLE, BOOKID +" = ?", new String[]{String.valueOf(book.getId())});
		db.close();
	}
}
