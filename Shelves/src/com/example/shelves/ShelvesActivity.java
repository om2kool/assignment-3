package com.example.shelves;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

public class ShelvesActivity extends Activity {
	protected SDBHelper db;
	List<Book> list;
	MyAdapter adapt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_shelves);
		db = new SDBHelper(this);
		list = db.getAllBooks();
		adapt = new MyAdapter(this, R.layout.list_shelves, list);
		ListView listBook = (ListView)findViewById(R.id.listBooks);
		listBook.setAdapter(adapt);
		
		Button b = (Button)findViewById(R.id.btnNew);
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ShelvesActivity.this, AddActivity.class);
				startActivity(i);			
			}
		});
		
		Button b1 = (Button)findViewById(R.id.btnUpdate);
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ShelvesActivity.this, UpdateActivity.class);
				startActivity(i);			
			}
		});
		
		Button b2 = (Button)findViewById(R.id.btnExit);
		b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shelves, menu);
		return true;
	}
	
	protected class MyAdapter extends ArrayAdapter<Book>
	{
	    Context context;
	    List<Book> bookList=new ArrayList<Book>();
	    int layoutResourceId;
	    public MyAdapter(Context context, int layoutResourceId,
	            List<Book> objects) {
	        super(context, layoutResourceId, objects);
	        this.layoutResourceId = layoutResourceId;
	        this.bookList=objects;
	        this.context=context;
	    }
	    
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	CheckBox chk = null;
			if (convertView == null) {
		        LayoutInflater inflater = (LayoutInflater) context
		            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		        convertView = inflater.inflate(R.layout.list_shelf, parent, false);
		        chk = (CheckBox)convertView.findViewById(R.id.chkSelect);
		        Button delb = (Button)convertView.findViewById(R.id.btnDelete);
		        convertView.setTag(chk);
		        chk.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						CheckBox cb = (CheckBox)v;						
						if(cb.isChecked()) {
							Book b = (Book)cb.getTag();														
						}
					}
				});
		        delb.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						CheckBox cb = (CheckBox)v;
						if(cb.isChecked()) {
							Book b = (Book)cb.getTag();
							db.removeBook(b);
						}						
					}
				});
		        
			} else {
				chk = (CheckBox) convertView.getTag();
			}
		    Book current = bookList.get(position);
		    chk.setText(current.getBookName());		    				 
	        return convertView;
	    }
	}										
}
